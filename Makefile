CC=			g++
CFLAGS=		-g -Wall -std=c++11
TARGETS=	run
MEASURE=	../common/measure
SHELL=		bash

all:		$(TARGETS)

clean:
	@echo Cleaning...
	rm -f $(TARGETS) *.o

run:		main.cpp song.h song.cpp merge.h merge.cpp
	@echo Compiling $@...
	@$(CC) $(CFLAGS) -o $@ $^

test:		test-output test-memory test-time

test-output:	run
	@echo Testing output...
	@diff --suppress-common-lines -y <(./run billboard100 rolling100) output

test-memory:	run
	@echo Testing memory...
	@[ `valgrind --leak-check=full ./run library1.txt library2.txt 2>&1 | grep ERROR | awk '{print $$4}'` = 0 ]

test-time: $(MEASURE) run
	@echo Testing time...
	@$(MEASURE) ./run library1.txt library2.txt | tail -n 1 | awk '{ if($$1 > 1.0) { print "Time limit exceeded"; exit 1} }'
