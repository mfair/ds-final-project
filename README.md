README.md
=========
Leigh Campbell, Marybeth Fair, Cailey Brogan
---------------------------------------------

This project is a way to merge two spotify playlists based on shared songs, sorted based on number of times played. 

Use
---
Required files:
For program:
song.cpp
song.h
merge.cpp
merge.h
main.cpp
Spotify libraries
Makefile
For server:
server folder with "spidey" code from systems (credit @ Bui)
www/script.sh CGI for running in the browser
For Spotify libraries:
go to http://joellehman.com/playlist/index.html
choose a playlist and copy list into an excel spreadsheet
remove commas, album names, and size
add a comma column after both song and artist
add a number of plays column (in a real setting we would get this information from Spotify's API)
copy into text file
Our Spotify libraries:
alana
annemarie
billboard100
cailey
emma
hailey
leigh
marybeth
memphis
old
rolling100
top50

To run in command line:
$ make
$ ./run <lib1> <lib2>

To run in browser:
$ cd server/
$ ./spidey -p 9555
in browser:
student02.cse.nd.edu:9555
type names of two libraries EXACTLY as they are named, then press submit

Benchmarking
------------
| Library 1            | Library 2            | Time          | Memory        |
|----------------------|----------------------|---------------|---------------|
| cailey               | leigh                | 0.470927      | 1.812500      |
| cailey               | marybeth             | 0.171973      | 1.601562      |
| cailey               | rolling100           | 0.009997      | 1.273438      |
| cailey               | billboard100         | 0.009997      | 1.277344      |
| cailey               | top50                | 0.008997      | 1.265625      |
| cailey               | memphis              | 0.071988      | 1.476562      |
| cailey               | old                  | 0.024995      | 1.355469      |
| cailey               | alana                | 0.010997      | 1.289062      |
| cailey               | annemarie            | 0.012997      | 1.308594      |
| cailey               | hailey               | 0.008998      | 1.269531      |
| cailey               | emma                 | 0.008997      | 1.265625      |
| leigh                | cailey               | 0.437932      | 1.812500      |
| leigh                | marybeth             | 0.628903      | 2.035156      |
| leigh                | rolling100           | 0.447931      | 1.757812      |
| leigh                | billboard100         | 0.447931      | 1.765625      |
| leigh                | top50                | 0.447931      | 1.753906      |
| leigh                | memphis              | 0.516921      | 1.933594      |
| leigh                | old                  | 0.454929      | 1.843750      |
| leigh                | alana                | 0.430933      | 1.781250      |
| leigh                | annemarie            | 0.434933      | 1.792969      |
| leigh                | hailey               | 0.442932      | 1.765625      |
| leigh                | emma                 | 0.445931      | 1.753906      |
| marybeth             | cailey               | 0.171973      | 1.601562      |
| marybeth             | leigh                | 0.608906      | 2.035156      |
| marybeth             | rolling100           | 0.164974      | 1.550781      |
| marybeth             | billboard100         | 0.158975      | 1.554688      |
| marybeth             | top50                | 0.164974      | 1.542969      |
| marybeth             | memphis              | 0.217966      | 1.722656      |
| marybeth             | old                  | 0.183971      | 1.632812      |
| marybeth             | alana                | 0.157975      | 1.566406      |
| marybeth             | annemarie            | 0.166973      | 1.585938      |
| marybeth             | hailey               | 0.155975      | 1.554688      |
| marybeth             | emma                 | 0.155975      | 1.546875      |
| rolling100           | cailey               | 0.008997      | 1.265625      |
| rolling100           | leigh                | 0.440932      | 1.761719      |
| rolling100           | marybeth             | 0.162975      | 1.546875      |
| rolling100           | billboard100         | 0.002999      | 1.210938      |
| rolling100           | top50                | 0.001999      | 1.203125      |
| rolling100           | memphis              | 0.067988      | 1.425781      |
| rolling100           | old                  | 0.018996      | 1.308594      |
| rolling100           | alana                | 0.003998      | 1.230469      |
| rolling100           | annemarie            | 0.005998      | 1.250000      |
| rolling100           | hailey               | 0.002998      | 1.214844      |
| rolling100           | emma                 | 0.001998      | 1.203125      |
| billboard100         | cailey               | 0.008997      | 1.269531      |
| billboard100         | leigh                | 0.420936      | 1.765625      |
| billboard100         | marybeth             | 0.164974      | 1.554688      |
| billboard100         | rolling100           | 0.002998      | 1.210938      |
| billboard100         | top50                | 0.002998      | 1.207031      |
| billboard100         | memphis              | 0.065989      | 1.429688      |
| billboard100         | old                  | 0.017996      | 1.308594      |
| billboard100         | alana                | 0.003998      | 1.238281      |
| billboard100         | annemarie            | 0.006998      | 1.250000      |
| billboard100         | hailey               | 0.002998      | 1.218750      |
| billboard100         | emma                 | 0.002998      | 1.203125      |
| top50                | cailey               | 0.008997      | 1.265625      |
| top50                | leigh                | 0.453930      | 1.757812      |
| top50                | marybeth             | 0.156975      | 1.542969      |
| top50                | rolling100           | 0.002998      | 1.199219      |
| top50                | billboard100         | 0.002998      | 1.207031      |
| top50                | memphis              | 0.067988      | 1.421875      |
| top50                | old                  | 0.017997      | 1.304688      |
| top50                | alana                | 0.003998      | 1.230469      |
| top50                | annemarie            | 0.005998      | 1.246094      |
| top50                | hailey               | 0.002998      | 1.207031      |
| top50                | emma                 | 0.001999      | 1.195312      |
| memphis              | cailey               | 0.072988      | 1.476562      |
| memphis              | leigh                | 0.512921      | 1.933594      |
| memphis              | marybeth             | 0.230964      | 1.722656      |
| memphis              | rolling100           | 0.063989      | 1.425781      |
| memphis              | billboard100         | 0.066989      | 1.433594      |
| memphis              | top50                | 0.066989      | 1.417969      |
| memphis              | old                  | 0.079986      | 1.503906      |
| memphis              | alana                | 0.068988      | 1.445312      |
| memphis              | annemarie            | 0.070988      | 1.460938      |
| memphis              | hailey               | 0.064989      | 1.425781      |
| memphis              | emma                 | 0.065989      | 1.425781      |
| old                  | cailey               | 0.023995      | 1.359375      |
| old                  | leigh                | 0.481925      | 1.843750      |
| old                  | marybeth             | 0.176972      | 1.632812      |
| old                  | rolling100           | 0.018996      | 1.308594      |
| old                  | billboard100         | 0.017996      | 1.312500      |
| old                  | top50                | 0.018996      | 1.300781      |
| old                  | memphis              | 0.083986      | 1.503906      |
| old                  | alana                | 0.018997      | 1.332031      |
| old                  | annemarie            | 0.021995      | 1.347656      |
| old                  | hailey               | 0.017996      | 1.316406      |
| old                  | emma                 | 0.017997      | 1.304688      |
| alana                | cailey               | 0.010997      | 1.289062      |
| alana                | leigh                | 0.454930      | 1.785156      |
| alana                | marybeth             | 0.161974      | 1.566406      |
| alana                | rolling100           | 0.004998      | 1.234375      |
| alana                | billboard100         | 0.003999      | 1.238281      |
| alana                | top50                | 0.003999      | 1.226562      |
| alana                | memphis              | 0.066989      | 1.441406      |
| alana                | old                  | 0.019996      | 1.328125      |
| alana                | annemarie            | 0.006998      | 1.269531      |
| alana                | hailey               | 0.003998      | 1.238281      |
| alana                | emma                 | 0.003998      | 1.226562      |
| annemarie            | cailey               | 0.012997      | 1.304688      |
| annemarie            | leigh                | 0.443931      | 1.792969      |
| annemarie            | marybeth             | 0.172973      | 1.582031      |
| annemarie            | rolling100           | 0.005998      | 1.246094      |
| annemarie            | billboard100         | 0.006998      | 1.257812      |
| annemarie            | top50                | 0.005998      | 1.246094      |
| annemarie            | memphis              | 0.068988      | 1.457031      |
| annemarie            | old                  | 0.020995      | 1.343750      |
| annemarie            | alana                | 0.007997      | 1.265625      |
| annemarie            | hailey               | 0.005998      | 1.253906      |
| annemarie            | emma                 | 0.005998      | 1.242188      |
| hailey               | cailey               | 0.009997      | 1.273438      |
| hailey               | leigh                | 0.454930      | 1.765625      |
| hailey               | marybeth             | 0.173972      | 1.554688      |
| hailey               | rolling100           | 0.002998      | 1.214844      |
| hailey               | billboard100         | 0.002999      | 1.218750      |
| hailey               | top50                | 0.002998      | 1.207031      |
| hailey               | memphis              | 0.066989      | 1.429688      |
| hailey               | old                  | 0.017996      | 1.312500      |
| hailey               | alana                | 0.004998      | 1.238281      |
| hailey               | annemarie            | 0.005998      | 1.253906      |
| hailey               | emma                 | 0.002998      | 1.210938      |
| emma                 | cailey               | 0.008998      | 1.265625      |
| emma                 | leigh                | 0.431934      | 1.753906      |
| emma                 | marybeth             | 0.165973      | 1.546875      |
| emma                 | rolling100           | 0.002998      | 1.203125      |
| emma                 | billboard100         | 0.002998      | 1.207031      |
| emma                 | top50                | 0.001998      | 1.195312      |
| emma                 | memphis              | 0.063989      | 1.421875      |
| emma                 | old                  | 0.016997      | 1.300781      |
| emma                 | alana                | 0.003998      | 1.226562      |
| emma                 | annemarie            | 0.005998      | 1.242188      |
| emma                 | hailey               | 0.001999      | 1.210938      |