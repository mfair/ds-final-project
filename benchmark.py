#!/usr/bin/env python3

import os
import time
import subprocess

# Variables

libraries = ["cailey", "leigh", "marybeth", "rolling100", "billboard100", "top50", "memphis", "old", "alana", "annemarie", "hailey", "emma"]

# Heading
print("| {:21}| {:21}| {:14}| {:14}|".format("Library 1", "Library 2", "Time", "Memory"))
print("|{:-<22}|{:-<22}|{:-<15}|{:-<15}|".format("-", "-", "-", "-"))

# Running
max_run_time = 60
FAILED = False

for lib1 in libraries:
	for lib2 in libraries:
		if lib1 == lib2:
			continue
		#Define Time
		start = time.time()
		end = start + max_run_time
		interval = max_run_time / 1000.0

		#Run Command
		command = "../common/measure ./run " + lib1 + " " + lib2 + " | tail -n 1 "

		p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)

		#Check if still waiting
		while True:
			result = p.poll()
			if result is not None:
				break
			if time.time() >= end:
				p.kill()
				print("| {:21}| {:21}| {:14}| {:14}|".format(lib1, lib2, "inf", "inf"))
				FAILED = True
				break
			time.sleep(interval)
		
		if not FAILED:
			stream, err  = p.communicate()#.stdout.read().split()
			stream = stream.decode().split()
			print("| {:21}| {:21}| {:14}| {:14}|".format(lib1, lib2, stream[0], stream[2]))

		FAILED = False