// readin.cpp
#include <iostream>
#include <string>
#include <stdio.h>
#include <ctype.h>
#include <vector>
#include <fstream>
#include <string.h>

#include "merge.h"

using namespace std;

void trim(std::string&);

int main(int argc, char* argv[]) {
	bool eval = false;
    string title,artist, nPlaysS;
    int nPlays;
    Library lib1;
	Library lib2;
	ifstream if1;
	ifstream if2;

	if1.open(argv[1]); //open two library files
	if2.open(argv[2]);
	if(!if1 || !if2){
		cout << "error. invalid file name" << endl;
		return 1;
	}
	while(if1.peek() != EOF){
		while(getline(if1, title, ',')){
			trim(title);
			getline(if1, artist, ',');
			trim(artist);
			getline(if1, nPlaysS, '\n');
			try { //in case format is wrong
				trim(nPlaysS);
				nPlays = stoi(nPlaysS);
			} catch(...) {
				continue;
			}
			lib1.push_front(artist, title, nPlays);
		}
	}

	while(if2.peek() != EOF){
		while(getline(if2, title, ',')){
			trim(title);
			getline(if2, artist, ',');
			trim(artist);
			getline(if2, nPlaysS, '\n');
			try {
				trim(nPlaysS);
				nPlays = stoi(nPlaysS);
			} catch(...){
				continue;
			}
			lib2.push_front(artist, title, nPlays);
		}
	}

	if(argc == 4) {
		eval = true;
	}

	merge_sort(lib1, 0); //sort based on title, then artist
	merge_sort(lib2, 0);
	merge_sort(lib1, 1);
	merge_sort(lib2, 1);

	if(eval) {
		cout << "1st Library, sorted: " << endl;
		lib1.print();
		cout << "*********************************" << endl;
		cout << "2nd Library, sorted: " << endl;
		lib2.print();
	}

	vector<Song*> playlist;
    Song* s1 = nullptr;
    Song* s2 = nullptr;

    s1 = lib1.pop();
    s2 = lib2.pop();

    int megaPrority = 0;
    while((lib1.size > 0) && (lib2.size > 0)) {
        if(s1->title == s2->title) {
            if(s1->artist == s2->artist) {
                megaPrority = s1->nPlays + s2->nPlays;
                s1->nPlays = megaPrority;
                addSong(playlist, s1);
                s1 = lib1.pop();
                s2 = lib2.pop();
            }else if(s1->artist < s2->artist) { 
                s1 = lib1.pop();
            } else if(s1->artist > s2->artist) {
                s2 = lib2.pop();
            }
        } else if(s1->title < s2->title) { 
            s1 = lib1.pop();
        } else if(s1->title > s2->title) {
            s2 = lib2.pop();
        }
    }

//	cout << "before output" << endl;

	cout << "<p>";

    for(size_t i=0; i<playlist.size(); i++) {    // pro;lly redundant
        cout << playlist[i]->title << " by " << playlist[i]->artist;
		if(i != playlist.size() - 1) cout << endl;
        delete playlist[i];
    }
	cout << "</p>" << endl;

    return 0;
}


void trim(std::string& s) //function to get rid of leading and trailing whitespace
{
	const char*t = " \t\n\r\f\v";
	s.erase(0, s.find_first_not_of(t));
	s.erase(s.find_last_not_of(t) + 1);
}
