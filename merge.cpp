// merge.cpp
#include <iostream>
#include <string>
#include <stdio.h>
#include <ctype.h>
#include <vector>
#include <fstream>
#include <string.h>

// #include "song.h"
#include "merge.h"

using namespace std;

void merge_sort(Library &l, int which) {
    l.head = msort(l.head, which);    // call recursive sort
}

Song *msort(Song *head, int which) {
    if(head == nullptr) return nullptr;     // if empty Library
    Song *left = nullptr;     // create heads for the two subLibrarys
    Song *right = nullptr;
    split(head, left, right);     // split into subLibrarys
    if(left->next != nullptr) {   // if subLibrary > 1, call msort on the subLibrary
        left = msort(left, which);
    }
    if(right->next != nullptr) {
        right = msort(right, which);
    }
	//if(left->title == right->title) cout << left->title << endl;
    head = merge(left, right, which);     // merge subLibrarys
	return head;
}

void split(Song *head, Song *&left, Song *&right) {
	Song* trash = head;
	int sz = 0;
	while(trash->next) {
		//cout << trash->title << endl;
		trash = trash->next;
		sz++;
	}
	trash = head;
	for(int i=0; i < sz - 1; i++) {
		trash=trash->next;
	}
	left = head;
	right = trash->next;
	trash->next = nullptr;
}

Song *merge(Song *left, Song *right, int which) {
    Song *head = nullptr;       // create head to return
    if(compare(left,right, which)) {     // if the first element of left is less than the first element of right
        head = left;    // then put that element first
        left = left->next;
    } else {      // otherwise, put the first right element first
        head = right;
        right = right->next;
    }
    Song *current = head;     // set current equal to head so you can traverse the Library without losing the head
    head = current;     // set head = current to return the head of the Library
    while(left && right) {    // while both Librarys are not empty
       // cout << "first merge while loop" << endl;
		 if(compare(left,right, which)) {       // go through and add the smallest element to the Library
            current->next = left;       // by setting current->next equal to it then incrementing current
            left = left->next;
        } else {
            current->next = right;
            right = right->next;
        }
        current = current->next;
    }
    while(left) {     // in case there's still elements left in left, just add them all
       	// cout << "second, left merge while loop" << endl;
		 current->next = left;
        left = left->next;
        current = current->next;
    }
    while(right) {
		// cout << "third, right merge while loop" << endl;
        current->next = right;
        right = right->next;
        current = current->next;
    }
    current = nullptr;      // add nullptr to the end
    return head;
}

bool compare(Song* left, Song* right, int which) {
    string leftS;
    string rightS;
    switch (which) {
        case 0: // artist
            leftS = left->artist;
            rightS = right->artist;
            break;
        case 1: // title
        default:    // title
            leftS = left->title;
            rightS = right->title;
            break;
    }
	//if(leftS == rightS) 
	//	if(which == 1)
	//		cout << "MERGE, L: " << leftS << " & R: " << rightS << ", with L: " <<  left->artist << " and R: " << right->artist << endl;
    if(leftS <= rightS) return true;
    return false;
}
