// merge.h
#include <iostream>
#include <string>
#include <stdio.h>
#include <ctype.h>
#include <vector>
#include <fstream>
#include <string.h>
using namespace std;

#include "song.h"

Song *msort(Song *head, int which);
void  split(Song *head, Song *&left, Song *&right);
Song *merge(Song *left, Song *right, int which);
bool compare(Song* left, Song* right, int which);
void merge_sort(Library &l, int which);
