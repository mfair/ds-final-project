/* handler.c: HTTP Request Handlers */

#include "spidey.h"

#include <errno.h>
#include <limits.h>
#include <string.h>

#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>

/* Internal Declarations */
HTTPStatus handle_browse_request(Request *request);
HTTPStatus handle_file_request(Request *request);
HTTPStatus handle_cgi_request(Request *request);
HTTPStatus handle_error(Request *request, HTTPStatus status);

/**
 * Handle HTTP Request.
 *
 * @param   r           HTTP Request structure
 * @return  Status of the HTTP request.
 *
 * This parses a request, determines the request path, determines the request
 * type, and then dispatches to the appropriate handler type.
 *
 * On error, handle_error should be used with an appropriate HTTP status code.
 **/
HTTPStatus  handle_request(Request *r) {
    HTTPStatus result=HTTP_STATUS_OK;

    /* Parse request */
    if (parse_request(r) == -1)
    {
        debug("HTTP_STATUS_BAD_REQUEST");
        handle_error(r, HTTP_STATUS_BAD_REQUEST);
        // If it's a failure, do you want to leave...?
        return HTTP_STATUS_BAD_REQUEST;
    }   
    
    /* Determine request path */
    char *dr = determine_request_path(r->uri);
    if (dr==NULL)
    {
        //handle_error(r, HTTP_STATUS_BAD_REQUEST);
        //return HTTP_STATUS_BAD_REQUEST;
        handle_error(r, HTTP_STATUS_NOT_FOUND);
        return HTTP_STATUS_NOT_FOUND;
    }
    r->path = dr;
    // free(dr);
    debug("HTTP REQUEST PATH: %s", r->path);
    struct stat sbuff;
    int status = stat(r->path, &sbuff);
    if (status<0)
    {
        debug("Stat Failed");
        handle_error(r, HTTP_STATUS_BAD_REQUEST);
        return HTTP_STATUS_BAD_REQUEST;
    }

    if((access(r->path, X_OK))==0 && !S_ISDIR(sbuff.st_mode)) //added is dir part because some weird things were happening
    {
        debug("CGI request: %s", r->path);
        result = handle_cgi_request(r);
    }
    else
    {
        if (S_ISREG(sbuff.st_mode))
        {
            debug("regular file: fiel request");
            result = handle_file_request(r);
        }
        else if (S_ISDIR(sbuff.st_mode))
        {
            debug("directory: handle browse request");
            result = handle_browse_request(r);
        }
        else
        {
            debug("invalid path ah");
            handle_error(r, HTTP_STATUS_BAD_REQUEST);
            return HTTP_STATUS_BAD_REQUEST;
        }
    }
    
    /* Dispatch to appropriate request handler type based on file type */
    
    log("HTTP REQUEST STATUS: %s", http_status_string(result));
    if(result != HTTP_STATUS_OK) handle_error(r, result);
    return result;
}

/**
 * Handle browse request.
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP browse request.
 *
 * This lists the contents of a directory in HTML.
 *
 * If the path cannot be opened or scanned as a directory, then handle error
 * with HTTP_STATUS_NOT_FOUND.
 **/
HTTPStatus  handle_browse_request(Request *r) {
    /* Open a directory for reading or scanning */
    DIR *d = opendir(r->path);
    if (!d)
    {
        debug("Directory: %s unable to open!", r->path);
        return HTTP_STATUS_INTERNAL_SERVER_ERROR;
    }
    debug("Directory opened");
    /* Write HTTP Header with OK Status and text/html Content-Type */
    fprintf(r->file, "HTTP/1.0 200 OK\r\n");
    fprintf(r->file, "Content-Type: text/html\r\n");
    fprintf(r->file, "\r\n");
    /* For each entry in directory, emit HTML list item */
    struct dirent **namelist;
    int sNum = scandir(r->path, &namelist, NULL, alphasort);
    if (sNum<0)
    {
        debug("directory: %s cannot be scanned", r->path);
        closedir(d);
        return HTTP_STATUS_INTERNAL_SERVER_ERROR;
    }
    // fprintf(r->file, "<ul>\r\n");
    fprintf(r->file, "<ul>");
    debug("Contents sent");
    for (int i=0; i<sNum; i++)
    {
        debug("HERE URI!!!: %s", r->uri);
        if(!strcmp(namelist[i]->d_name,".")) { // || !strcmp(namelist[i]->d_name,"..")
            free(namelist[i]);
            continue;
        }
        if (streq(r->uri, "/"))
        {
            debug("uri is \\   link is <li><a href = \"%s%s\">%s</a></li>\r\n", r->uri, namelist[i]->d_name, namelist[i]->d_name);
            fprintf(r->file, "<li><a href=\"%s%s\">%s</a></li>\r\n", r->uri, namelist[i]->d_name, namelist[i]->d_name);
        }
        else
        {
            fprintf(r->file, "<li><a href=\"%s/%s\">%s</a></li>\r\n", r->uri, namelist[i]->d_name, namelist[i]->d_name);
        }
        free(namelist[i]);
    }
    free(namelist);
    fprintf(r->file, "</ul>\r\n");
    /* Flush socket, return OK */
    fflush(r->file);
    closedir(d);
    return HTTP_STATUS_OK;
}


/**
 * Handle file request.
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP file request.
 *
 * This opens and streams the contents of the specified file to the socket.
 *
 * If the path cannot be opened for reading, then handle error with
 * HTTP_STATUS_NOT_FOUND.
 **/
HTTPStatus  handle_file_request(Request *r) {
    debug("Entered handle file erquest fn");
    FILE *fs;
    char buffer[BUFSIZ] = "Marybeth";
    char *mimetype = NULL;
    size_t nread = 0;
    
    if(access(r->path, F_OK) == 0) {    // for debugging
        debug("heyyyyyyyyy ezista.");
    }

    /* Open file for reading */
    fs = fopen(r->path, "r");
    if(fs == NULL) {
        // fprintf(stderr, "yikes fopen failed: %s\n", strerror(errno));
        fprintf(stderr, "yikes fopen failed!");
        goto fail;
    }
    /* Determine mimetype */
    debug("Path: %s", r->path);
    mimetype = determine_mimetype(r->path); // yikes
    // mimetype = "image/jpeg";
    debug("Mimetype: %s", mimetype);
    
    /* Write HTTP Headers with OK status and determined Content-Type */
    fprintf(r->file, "HTTP/1.0 200 OK\r\n"); // what
    fprintf(r->file, "Content-Type: %s\r\n", mimetype);
    fprintf(r->file, "\r\n");

    /* Read from file and write to socket in chunks */
    while((nread = fread(buffer, 1, BUFSIZ, fs)) > 0) {
        debug("Writing: %s", buffer);
        fwrite(buffer, 1, nread, r->file); // y
    }
    
    /* Close file, flush socket, deallocate mimetype, return OK */
    fflush(fs);
    fflush(r->file); //not sure if this needs to happen but thats the soocket?
    fclose(fs);
    free(mimetype);
    return HTTP_STATUS_OK;

fail:
    /* Close file, free mimetype, return INTERNAL_SERVER_ERROR */
    if(fs) fclose(fs);
    if(mimetype) free(mimetype);
    return HTTP_STATUS_INTERNAL_SERVER_ERROR;
}

/**
 * Handle CGI request
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP file request.
 *
 * This popens and streams the results of the specified executables to the
 * socket.
 *
 * If the path cannot be popened, then handle error with
 * HTTP_STATUS_INTERNAL_SERVER_ERROR.
 **/
HTTPStatus handle_cgi_request(Request *r) {
    FILE *pfs;
    char buffer[BUFSIZ];

    /* Export CGI environment variables from request structure:
     * http://en.wikipedia.org/wiki/Common_Gateway_Interface */
    setenv("DOCUMENT_ROOT", RootPath, 1); //or r->path?? idkk
    if(r->query) setenv("QUERY_STRING", r->query, 1); //
    else setenv("QUERY_STRING", "Marybeth Rulez", 1);
    if(r->port) setenv("REMOTE_PORT", r->port, 1);
    if(r->method) setenv("REQUEST_METHOD", r->method, 1);
    if(r->uri) setenv("REQUEST_URI", r->uri, 1);
    if(r->path) setenv("SCRIPT_FILENAME", r->path, 1);
    if(r->port) setenv("SERVER_PORT", r->port, 1); //same as remote port?
    // setenv("REMOTE_HOST", r->host, 1);
    if(r->host) setenv("REMOTE_ADDR", r->host, 1);

    for (struct header *header = r->headers; header!=NULL; header=header->next)
    {
        if (streq(header->name, "Host"))
            setenv("HTTP_HOST", header->value, 1);
        else if (streq(header->name, "Accept"))
            setenv("HTTP_ACCEPT", header->value, 1);
        else if (streq(header->name, "Accept-Language"))
            setenv("HTTP_ACCEPT_LANGUAGE", header->name, 1);
        else if (streq(header->name, "Accept-Encoding"))
            setenv("HTTP_ACCEPT_ENCODING", header->value, 1);
        else if (streq(header->name, "Connection"))
            setenv("HTTP_CONNECTION", header->value, 1);
        else if (streq(header->name, "User-Agent"))
            setenv("HTTP_USER_AGENT", header->value, 1);
        else
            continue;
        debug("Envirnmental variable %s:%s set", header->name, header->value);
    }
    /* Export CGI environment variables from request headers */

    /* POpen CGI Script */
    pfs = popen(r->path, "r");  //not entirely sure if that is the path I send or something else ans I think we should send 'r'
    /* Copy data from popen to socket */
    while (fgets(buffer, BUFSIZ, pfs))
    {
        fputs(buffer, r->file);
    }
    /* Close popen, flush socket, return OK */
    pclose(pfs);
    fflush(r->file);
    return HTTP_STATUS_OK;
}

/**
 * Handle displaying error page
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP error request.
 *
 * This writes an HTTP status error code and then generates an HTML message to
 * notify the user of the error.
 **/
HTTPStatus  handle_error(Request *r, HTTPStatus status) {
    debug("I know there is an error, and I am attempting to contact the client regarding this.");
    const char *status_string = http_status_string(status);

    /* Write HTTP Header */
     fprintf(r->file, "HTTP/1.0 %s\r\n", status_string);
     // fprintf(r->file, "HTTP/1.0 200 OK");
     fprintf(r->file, "Content-Type: text/html\r\n");
     fprintf(r->file, "\r\n");
    fprintf(r->file, "<p>HTTP error: %s! Yikes! yikesyikesyikes</p>\r\n", status_string); // html
     fflush(r->file);
    
    /* Write HTML Description of Error*/

    /* Return specified status */
    debug("i SUPPOSEDLY printted to the screen smh.");
    return status;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
