/* request.c: HTTP Request Functions */

#include "spidey.h"

#include <errno.h>
#include <string.h>

#include <unistd.h>

int parse_request_method(Request *r);
int parse_request_headers(Request *r);

/**
 * Accept request from server socket.
 *
 * @param   sfd         Server socket file descriptor.
 * @return  Newly allocated Request structure.
 *
 * This function does the following:
 *
 *  1. Allocates a request struct initialized to 0.
 *  2. Initializes the headers list in the request struct.
 *  3. Accepts a client connection from the server socket.
 *  4. Looks up the client information and stores it in the request struct.
 *  5. Opens the client socket stream for the request struct.
 *  6. Returns the request struct.
 *
 * The returned request struct must be deallocated using free_request.
 **/
Request * accept_request(int sfd) {
    Request *r;
    struct sockaddr raddr;
    socklen_t rlen = sizeof(struct sockaddr);
    debug("in accepting request fn");
    /* Allocate request struct (zeroed) */
    r = calloc(1, sizeof(Request));
    debug("PORT IS: %s", Port);
    debug("Waiting to accept...");
    /* Accept a client */
    int client_fd = accept(sfd, &raddr, &rlen);
    debug("Client fd: %d", client_fd);
    if(client_fd < 0) {
        // fprintf(stderr, "Unable to accept: %s\n", strerror(errno));
        fprintf(stderr, "Unable to accept!\n");
		goto fail;
    }

    /* Lookup client information */
    // getting not sending info!!!!
    socklen_t hostlen = NI_MAXHOST;
    socklen_t servlen = NI_MAXHOST;
    char host[NI_MAXHOST];
    char serv[NI_MAXHOST];
	if(getnameinfo(&raddr, rlen, host, hostlen, serv, servlen, NI_NUMERICHOST)) {
		fprintf(stderr, "Problem with getnameinfo: %s\n", strerror(errno));
		goto fail;
	}
    debug("Original Host: %s, Original Serv: %s", host, serv);


    /* Open socket stream */
	FILE* socketStream = fdopen(client_fd, "w+");
	if(socketStream == NULL) {
        fprintf(stderr, "Problem with opening socket stream.........\n");
        goto fail;
	}

	r->fd = client_fd;
    r->file = socketStream;
    *r->host = *host;
    //r->port = ???? here
    // but where do i get it
	*r->port = *Port;
    
    debug("Host: %s", r->host);
    debug("Port: %s", r->port);
	
    log("Accepted request from %s:%s", r->host, r->port);	// access system wide log messages
	
	return r;

fail:	// for goto??
    debug("Failed.");
    /* Deallocate request struct */
	free_request(r);
    return NULL;
}

/**
 * Deallocate request struct.
 *
 * @param   r           Request structure.
 *
 * This function does the following:
 *
 *  1. Closes the request socket stream or file descriptor.
 *  2. Frees all allocated strings in request struct.
 *  3. Frees all of the headers (including any allocated fields).
 *  4. Frees request struct.
 **/
void free_request(Request *r) {
    if (!r) {
        fprintf(stderr, "CANT free something that doesn't exist.....\n");
    	return;
    }

    /* Close socket or fd */
    if(r->fd) close(r->fd); // y not fdclose
    debug("Closed socket.");

    /* Free allocated strings */
    if(r->method)   {
        free(r->method);
        debug("Freed method!");
    }
    if(r->uri)  {
        free(r->uri);
        debug("Freed uri!");
    }
    if(r->path) { // ????????
        free(r->path);
        debug("Freed path?");
    }
    if(r->query)    {
        free(r->query);
        debug("Freed query!");
    }
    debug("Freed all allocated strings.");

    /* Free headers */
    
    struct header* curr = r->headers;
    struct header* next = r->headers; // ?
    while(curr) {
        next = curr->next;
        debug("Freeing headers.");
        free(curr->name);
        free(curr->value);
        free(curr);
        curr = next;
    }
    /* Free request */
    free(r);
    debug("Freed request");
}

/**
 * Parse HTTP Request.
 *
 * @param   r           Request structure.
 * @return  -1 on error and 0 on success.
 *
 * This function first parses the request method, any query, and then the
 * headers, returning 0 on success, and -1 on error.
 **/
int parse_request(Request *r) {
    /* Parse HTTP Request Method */
		debug("RootPath here: %s", RootPath);
    if(parse_request_method(r) != 0) {
        fprintf(stderr, "Unable to parse request method\n");
        return -1;
    }
    debug("Parsd request method.");
		debug("RootPath here: %s", RootPath);

    /* Parse HTTP Requet Headers*/
    if(parse_request_headers(r) != 0) {
        fprintf(stderr, "Unable to parse request headers\n");
        return -1;
    }
    debug("Parsed request header.");
    debug("RootPath here: %s", RootPath);
    return 0;
}

/**
 * Parse HTTP Request Method and URI.
 *
 * @param   r           Request structure.
 * @return  -1 on error and 0 on success.
 *
 * HTTP Requests come in the form
 *
 *  <METHOD> <URI>[QUERY] HTTP/<VERSION>
 *
 * Examples:
 *
 *  GET / HTTP/1.1
 *  GET /cgi.script?q=foo HTTP/1.0
 *
 * This function extracts the method, uri, and query (if it exists).
 **/

/* You can use skip_whitespace in conjunction with strtok to parse the method and URI.
    why
    You can use strchr to help you parse the query from the URI.
    why
    You should use strdup to store strings in the request struct.
    why
 */
int parse_request_method(Request *r) {      // method, uri, query
		debug("RootPath HERE: %s", RootPath);
    char buffer[BUFSIZ];
    //char *method;     y is these
    //char *uri;
    //char *query;
		debug("RootPath HERE: %s", RootPath);
    /* Read line from socket */
    if(fgets(buffer, BUFSIZ, r->file) == NULL) {
        debug("fgets failure in parse_request_method");
        fprintf(stderr, "fgets failed!\n");
        goto fail;
    }
	debug("BUFFER in request method: %s",buffer);
 
    /* Parse method and uri */
    char* SPACE = " ";
    // char* QUESTIONMARK = "?";
    char* tmp = strtok(buffer, SPACE);
    r->method = strdup(tmp);
    // r->method = strtok(buffer, SPACE);
    char* tmp2 = strtok(NULL, SPACE);
    if(tmp2 == NULL) goto fail;
		if (strchr(tmp2, '?'))
		{
				char *remove = strchr(tmp2, '?');
				r->query = strdup(remove+1);
				*remove = '\0';
		}
	
	debug("**URI becoming: %s", tmp2);
    r->uri = strdup(tmp2);
    //r->uri = strtok(NULL, SPACE);
    // Parse query from uri - why would i do that
    /*char* query = strchr(r->uri, '?');
	char* tmp = query;
	query++;
	*tmp = 0;*/
    // r->query = strtok(NULL, SPACE);
    // chomp(r->query);
    //char* tmp3 = strtok(NULL, SPACE);
    //r->query = strdup(tmp3);
    /* Record method, uri, and query in request struct */

    debug("HTTP METHOD: %s", r->method);
    debug("HTTP URI:    %s", r->uri);
    debug("HTTP QUERY:  %s", r->query);

    return 0;

fail:
    debug("Failed.");
    return -1;
}

/**
 * Parse HTTP Request Headers.
 *
 * @param   r           Request structure.
 * @return  -1 on error and 0 on success.
 *
 * HTTP Headers come in the form:
 *
 *  <NAME>: <VALUE>
 *
 * Example:
 *
 *  Host: localhost:8888
 *  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:29.0) Gecko/20100101 Firefox/29.0
 *  Accept: text/html,application/xhtml+xml
 *  Accept-Language: en-US,en;q=0.5
 *  Accept-Encoding: gzip, deflate
 *  Connection: keep-alive
 *
 * This function parses the stream from the request socket using the following
 * pseudo-code:
 *
 *  while (buffer = read_from_socket() and buffer is not empty):
 *      name, value = buffer.split(':')
 *      header      = new Header(name, value)
 *      headers.append(header)
 **/

/*
 You should use chomp, skip_whitespace, and strchr to parse the headers.
 
 You should use calloc to allocate new header struct.
*/
int parse_request_headers(Request *r) {
    debug("Entered Parse Rquest Headers");
    struct header *curr = NULL;
    char buffer[BUFSIZ];
    char *name;
    char *value;
    debug("Allocated stuff1");
    

    /* Parse headers from socket */
    while(fgets(buffer, BUFSIZ, r->file) && buffer != NULL) {
        // debug("*********** while *************");
        // if(curr) debug("@start curr name is %s", curr->name);
        debug("at beginning of while RootPath is: %s", RootPath);
        if(!(strcmp(buffer,"\n")&&strcmp(buffer,"\r\n"))) break;     // skip that last newline
        // Header* new = calloc(1, sizeof(Header));    // create a new header node
        curr = calloc(1, sizeof(Header));
        // if(new == NULL) {
        if(curr == NULL) {
            fprintf(stderr, "CALLOC FAIIIIIILED");
            goto fail;
        }
        // debug("@start New name is %s", new->name);
        char* COLON = ":";      // for strtok
        char* NEWLINE = "\n";       // for strok
        debug("Buffer is %s", buffer);
        name = strtok(buffer, COLON);       // get name
        // new->name = name;           // save name to new
        // new->name = strdup(name);
        curr->name = strdup(name);
        debug("Name is: %s", name);
        value = strtok(NULL, NEWLINE);      // get the value by searching until newline
        if(value == NULL) {
            free(curr->name);
            free(curr);
            goto fail;
        }
        // do we chomp here?
        value = skip_whitespace(value);         // skip the start whitespace
        // debug("White-speace-skipped buffer: %s", new->value);
        // new->value = value;     // save the value to new
        //new->value = strdup(value);
        curr->value = strdup(value);
        curr->next = r->headers;
        r->headers = curr;
        /*if(curr != NULL) {      // not the first one added
            debug("Not first");
            debug("Test: curr->name is %s", curr->name);        // SHOULD be the name of the previous right???!!!???!??!?!?!?!?!?!?!?
            curr->next = new;
            curr = curr->next;
        } else {            // the first one added
            debug("First");
            curr = new;
            debug("CURR: name %s", curr->name);
            r->headers = curr;
        }*/
		
    }
    if(r->headers == NULL) {
        debug("Pointer is null");
        goto fail;
    }

#ifndef NDEBUG
    for (struct header *header = r->headers; header != NULL; header = header->next) {
        // why are you doing struct header and not Header
    	debug("HTTP HEADER %s = %s", header->name, header->value);
    }
#endif
    return 0;

fail:
    return -1;
}
