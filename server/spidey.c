/* spidey: Simple HTTP Server */

#include "spidey.h"

#include <errno.h>
#include <stdbool.h>
#include <string.h>

#include <unistd.h>

/* Global Variables */
// Bri don't delete our files plz
char *Port	          = "9898";
char *MimeTypesPath   = "/etc/mime.types";
char *DefaultMimeType = "text/plain";
char *RootPath	      = "www";
ServerMode mode = SINGLE;

/**
 * Display usage message and exit with specified status code.
 *
 * @param   progname    Program Name
 * @param   status      Exit status.
 */
void usage(const char *progname, int status) {
    fprintf(stderr, "Usage: %s [hcmMpr]\n", progname);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "    -h            Display help message\n");
    fprintf(stderr, "    -c mode       Single or Forking mode\n");
    fprintf(stderr, "    -m path       Path to mimetypes file\n");
    fprintf(stderr, "    -M mimetype   Default mimetype\n");
    fprintf(stderr, "    -p port       Port to listen on\n");
    fprintf(stderr, "    -r path       Root directory\n");
    exit(status);
}

/**
 * Parse command-line options.
 *
 * @param   argc        Number of arguments.
 * @param   argv        Array of argument strings.
 * @param   mode        Pointer to ServerMode variable.
 * @return  true if parsing was successful, false if there was an error.
 *
 * This should set the mode, MimeTypesPath, DefaultMimeType, Port, and RootPath
 * if specified.
 */
bool parse_options(int argc, char *argv[], ServerMode *mode) {
    int argind = 1;
    char* PROGNAME = argv[0];
    char* arg = argv[argind++];
    debug("arg %s", arg);
    while(argind < argc+1) {
        debug("in while");
        if(!strcmp(arg, "-h")) {
            debug("Calling help...");
            usage(PROGNAME, 0);
        }
        else if(!strcmp(arg, "-c")) {
            arg = argv[argind++];
            if(!strcmp(arg, "single")) {
                *mode = SINGLE;
            } else if(!strcmp(arg, "forking")) {
                *mode = FORKING;
            } else {
                debug("INVALID MODE: %s", arg);
                usage(PROGNAME, 1);
            }
            debug("Mode = %s", arg);
        }
        else if(!strcmp(arg, "-m")) {
            arg = argv[argind++];
            MimeTypesPath = arg;
            debug("MimeTypes Path = %s", MimeTypesPath);
        } else if(!strcmp(arg, "-M")) {
            arg = argv[argind++];
            DefaultMimeType = arg;
            debug("DefaultMimeType = %s", DefaultMimeType);
        } else if(!strcmp(arg, "-p")) {
            arg = argv[argind++];
            Port = arg;
            debug("Port = %s", Port);
        } else if(!strcmp(arg, "-r")) {
            arg = argv[argind++];
            RootPath = arg;
            debug("RootPath = %s", RootPath);
        } else {
            debug("Error!!!!!!");
            usage(PROGNAME, 1);
        }
        arg = argv[argind++];
    }
    char tmp[BUFSIZ];
    if(realpath(RootPath, tmp) == NULL) {
        fprintf(stderr, "Realpath issue!");
        return false;
    }
    RootPath = strndup(tmp, BUFSIZ);    // do we free this anywhere
    debug("Rootpath actual::: %s", RootPath);
    return true;
}

/**
 * Parses command line options and starts appropriate server
 **/
int main(int argc, char *argv[]) {
    /* Parse command line options */
    parse_options(argc, argv, &mode);
    //debug("%s", determine_mimetype("hahaha.jpg"));
    /* Listen to server socket */
    int sfd;
    if((sfd = socket_listen(Port)) == -1) {
        fprintf(stderr, "socket cant lsiten\n");
        return EXIT_FAILURE;
    }

    /* Determine real RootPath */
    // done in parse command line options!

    log("Listening on port %s", Port);
    debug("RootPath        = %s", RootPath);
    debug("MimeTypesPath   = %s", MimeTypesPath);
    debug("DefaultMimeType = %s", DefaultMimeType);
    debug("ConcurrencyMode = %s", mode == SINGLE ? "Single" : "Forking");

    /* Start either forking or single HTTP server */
    if (mode==SINGLE)   // TODO (mayyybe error check)
    {
        single_server(sfd);
    }
    else if (mode==FORKING)
    {
        forking_server(sfd);
    }
    return EXIT_SUCCESS;

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
}
