/* utils.c: spidey utilities */

#include "spidey.h"

#include <ctype.h>
#include <errno.h>
#include <string.h>

#include <sys/stat.h>
#include <unistd.h>

/**
 * Determine mime-type from file extension.
 *
 * @param   path        Path to file.
 * @return  An allocated string containing the mime-type of the specified file.
 *
 * This function first finds the file's extension and then scans the contents
 of tme MimeTypesPath file to determine which mimetype the file has.
 *
 * The MimeTypesPath file (typically /etc/mime.types) consists of rules in the
 * following format:
 *
 *  <MIMETYPE>      <EXT1> <EXT2> ...
 *
 * This function simply checks the file extension version each extension for
 * each mimetype and returns the mimetype on the first match.
 *
 * If no extension exists or no matching mimetype is found, then return
 * DefaultMimeType.
 *
 * This function returns an allocated string that must be free'd.
 **/
char * determine_mimetype(const char *path) {
    char *ext;
    char *mimetype;
    char *token="\n";
    char buffer[BUFSIZ];
    FILE *fs = NULL;

    /* Find file extension */
    ext = strrchr(path, '.')+1;
    if (ext==NULL)
    {
        return DefaultMimeType;
    }
    /* Open MimeTypesPath file */
    char *mimeFilePath = "/etc/mime.types";
    fs = fopen(mimeFilePath, "r");
    if (fs==NULL)
    {
        fprintf(stderr, "Error opening %s: %s\n", mimeFilePath, strerror(errno));
        return DefaultMimeType;
    }
    char mimetypeSEND[BUFSIZ]="";
    /* Scan file for matching file extensions */
    while(fgets(buffer, BUFSIZ, fs))
    {
        char *line = strtok(buffer, token);
        if (!line)
            continue;
        char *subLine;
        subLine = skip_nonwhitespace(line);
        subLine = skip_whitespace(subLine);
        while (subLine &&subLine+1 && (*subLine)!='\0' && *(subLine+1)!='\0')
        {
            char *remove = skip_nonwhitespace(subLine);
            char *next = remove+1;
            *remove = '\0';
            if (subLine&&streq(subLine, ext))
            {       
                remove = skip_nonwhitespace(line);
                *remove = '\0';
                strncpy(mimetypeSEND, line, BUFSIZ);
                break;
            }
            subLine = next;
            
        }
    }
    mimetype = strdup(mimetypeSEND);
    if (streq(mimetype, ""))
    {
        debug("Default MimeType returned");
        mimetype = strdup(DefaultMimeType);
        return mimetype;
    }
    else
    {
        debug("Mime type successfully determined");
        return mimetype;
    }
}

/**
 * Determine actual filesystem path based on RootPath and URI.
 *
 * @param   uri         Resource path of URI.
 * @return  An allocated string containing the full path of the resource on the
 * local filesystem.
 *
 * This function uses realpath(3) to generate the realpath of the
 * file requested in the URI.
 *
 * As a security check, if the real path does not begin with the RootPath, then
 * return NULL.
 *
 * Otherwise, return a newly allocated string containing the real path.  This
 * string must later be free'd.
 **/
char * determine_request_path(const char *uri) {
    debug("uri st request path start: %s", uri);
    char fulluri[BUFSIZ];
    snprintf(fulluri, BUFSIZ, "%s%s", RootPath, uri);
    debug("uri changed to %s", fulluri);
    //char *resolvedPath = calloc(BUFSIZ, sizeof(char));
    char resolvedPath[BUFSIZ];
    debug("uri: %s full uri: %s", uri, fulluri);
    if (realpath(fulluri, resolvedPath)==NULL)
    {
        debug("Path not found");
        // free(resolvedPath);
        // free(resolvedPath);
        return NULL;
    }
    else if (streq(strstr(resolvedPath, RootPath), resolvedPath))
    {
        debug("Path found and valid: %s!", resolvedPath);
        return strdup(resolvedPath);
    }
    else
    {
        // free(resolvedPath);
        debug("Path not in RootPath!!");
        // free(resolvedPath);
        return NULL;
    }
}

/**
 * Return static string corresponding to HTTP Status code.
 *
 * @param   status      HTTP Status.
 * @return  Corresponding HTTP Status string (or NULL if not present).
 *
 * http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
 **/
const char * http_status_string(HTTPStatus status) {
    static char *StatusStrings[] = {
        "200 OK",
        "400 Bad Request",
        "404 Not Found",
        "500 Internal Server Error",
        "418 I'm A Teapot",
    };
    debug("Returning HTTP status: %s", StatusStrings[status]);
    return StatusStrings[status];
}

/**
 * Advance string pointer pass all nonwhitespace characters
 *
 * @param   s           String.
 * @return  Point to first whitespace character in s.
 **/
char * skip_nonwhitespace(char *s) {
    while(*s!='\0'&&*s!=' '&&*s!='\t'&&*s!='\n')
        s++;
    return s;
}

/**
 * Advance string pointer pass all whitespace characters
 *
 * @param   s           String.
 * @return  Point to first non-whitespace character in s.
 **/
char * skip_whitespace(char *s) {
    while (*s!='\0'&&(*s==' '||*s=='\t'||*s=='\n'))
    {
        s++;
    }
    return s;
}


/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
