#!/bin/sh

echo "HTTP/1.0 200 OK"
echo "Content-type: text/html"
echo

LIB1=$(echo $QUERY_STRING | sed -En 's|.*message1=([^&]*).*|\1|p' | sed 's/+/ /g')
LIB2=$(echo $QUERY_STRING | sed -En 's|.*message2=([^&]*).*|\1|p' | sed 's/+/ /g')

cat <<EOF
<h1>Our Playist Generator</h1>
<hr>
<form>
<input type="text" name="message1" value="$LIB1">
<input type="text" name="message2" value="$LIB2">
EOF

cat <<EOF
</select>
<input type="submit">
</form>
<hr>
EOF

LIB1=/afs/nd.edu/user1/mfair/SP18/cse20312/data_structures/project-code/final/$LIB1
LIB2=/afs/nd.edu/user1/mfair/SP18/cse20312/data_structures/project-code/final/$LIB2

if [ -n "$LIB2" ]; then
cat <<EOF
<pre>
$(/afs/nd.edu/user1/mfair/SP18/cse20312/data_structures/project-code/final/run "$LIB1" "$LIB2")
</pre>
EOF

fi

