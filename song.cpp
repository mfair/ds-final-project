// song.cpp
#include <iostream>
#include <string>
#include <stdio.h>
#include <ctype.h>
#include <vector>
#include <fstream>
#include <string.h>

#include "song.h"

using namespace std;

Song* Library::pop() {
    Song* tmp = head;
    head = head->next;
    size--;
    return tmp;
}

Library::Library() {
    head = nullptr;
	size = 0;
}

Library::~Library() {
    Song* next = nullptr;
    for(Song* current = head; current != nullptr; current = next) {
        next = current->next;
        delete current;
    }
}

void Library::push_front(const string &a, const string &t, const int &n) {
	if(head == nullptr) head = new Song{a, t, n, nullptr, nullptr, nullptr};
	else{
   		 head = new Song{a, t, n, head, nullptr, nullptr};
	}
	size++;
}

void Library::print() {
	Song* tmp = head;
	while(tmp) {
		cout << "Title: " << tmp->title << ", Artist: " << tmp->artist << ", Number of Plays: " << tmp->nPlays << endl;
		tmp = tmp->next;
	}
}

void addSong(vector<Song*> &playlist, Song* song) {
    playlist.push_back(song);
    int i = playlist.size() - 1;
    while(i > 0) {
        Song* current = playlist[i];
        Song* parent = playlist[PARENT(i)];
        if(current->nPlays <= parent->nPlays) break;
        playlist[i] = parent;
        playlist[PARENT(i)] = current;
        i = PARENT(i);
    }
}
