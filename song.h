// song.h
#include <iostream>
#include <string>
#include <stdio.h>
#include <ctype.h>
#include <vector>
#include <fstream>
#include <string.h>
using namespace std;
#define PARENT(i)       ((i-1)/2)
#define LEFT_CHILD(i)   (2*(i) + 1)
#define RIGHT_CHILD(i)  (2*(i) + 2)

struct Song {
    string artist;
    string title;
    int nPlays;
    Song* next;
    Song* right;
    Song* parent;
};

struct Library {
    Song* head;
    int size;
    Library();
    ~Library();
    void push_front(const string &a, const string &t, const int &n);
    void print();
    Song* pop();
};

void addSong(vector<Song*> &playlist, Song* song);
